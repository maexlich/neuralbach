\chapter{Experiments}

\section{Data}
\subsection{Polyphonic piano music}
For the first part dealing with polyphonic piano music, we extract information from MIDI files containing all Goldberg Variations of Bach from \url{http://www.bachcentral.com/}.%, as well as all Beethoven Sonatas from \url{http://www.dongrays.com/midi/archive/clas/beet/}.
In these files, the right and left hand are usually seperated quite well. When there are more than two tracks in a file, we combine tracks by hand using the GUI tool Rosegarden, such that there are two tracks left with the almost always correct assignment of hands.
The pitch of notes in MIDI files is included as integers for every half-tone, where the C4 on a piano keyboard is equivalent to pitch 60. 
For every time slice of 1/8th of a quarter note, we construct a boolean 'one-hot vector' of size 100. When a note is played at that time, we fill in a one, otherwise a zero. Note that the vector is not really a 'one-hot' vector, since several notes can be played simultaneously. 
From the Bach dataset, we extract 8000 random timeslices of length 256 (corresponding to 32 quarter notes), which we use as training data. 
This kind of dataset can not distinguish between a note that is held for several timesteps, or pressed again and again. We choose this to simplify the problem. Future work could include two matrices, one containing ones if a tone is held, and one containing a one when a tone is started (similar to \cite{composemusic}).
To show how the trained model behaves on very different data, we also transformed the main voice of the 'Ode to Joy' into a similar matrix.
We make a 95/5 split between training- and testset.

\subsection{Bach Chorales}
For the second part of our experiments, we use a set of 371 Bach chorales. We first found them as a often used dataset in the UCI Machine Learning Repository (\url{https://archive.ics.uci.edu/ml/datasets/Bach+Chorales}), but the 4 voices are not seperated there. 
The same 371 chorales are available on \url{http://kern.humdrum.org/cgi-bin/browse?l=users/craig/classical/bach/371chorales} as HUMDRUM files, which is a file format for representing sheet music. The 4 voices are seperate columns. We use the (very powerful) music21 package to parse HumDrum files. Every file also has the key annotated, so we easily transposed our data to C major. Like in the piano dataset before, we construct a boolean 'one-hot vector' for every voice. To reduce the amount of data and also the complexity of the trained model, we subtract 26 from the midi pitches, since the range of pitches in the chorales is from 27 to 81. By that, our one-hot vector is just of size 55.
Since the chorales have less temporal complexity than the polyphonic piano music, we choose to use a temporal resolution of 1/4th of a quarter note.

We alternatively formulate the data not by a one-hot vector, but just by one integer per timeslice and voice, denoting the midi pitch. While the first formulation can be seen as a classification problem, the second one is a regression problem.

We zero-padded the chorales to the length of the longest choral, so all sequences have a resulting padded length of 772 timesteps.

All processed data is available in our repository "neuralbach" unter '/data/processed'.


\section{Framework}

Since our project involves testing of many different neural network architectures, we choose to use the high-level framework Keras to build our models \cite{keras}. Keras is built on top of both the TensorFlow and the Theano backend. It allows an abtract definition of NN architectures. One possiblity is to define a sequential model of Layers (sequential API), where the output of one layer is directly fed into the next. The other possibility is a functional API, where layers are callable with tensors, thus making it possible to have multiple outputs or inputs into layers.

Keras already implements a lot of layers like Dense Layers, Recurrent Layers, Concolutional Layers and various Activation Layers. Its functionality is not fixed to already implemented layers, it is possible to write own extensions with arbitrary functionality. Keras also provides simple visualisation of architectures, which is used here in the protocol.

We trained our models on a Tesla Kepler K40 GPU.








\newpage 
\section{Prediction of 2nd Voice of Polyphonic Piano Music}

We use LSTMs to produce $\hat{y}_t$ (left hand) from $X_t$ (right hand) for every t in a sequence of polyphonic piano inputs. We try out three different network architectures, going from simple to more complex.


\subsection{Vanilla LSTM}

As a baseline for more complex models later, we use a single layer LSTM, here called the Vanilla LSTM. 
The visualisation provided by Keras is shown in Figure \ref{fig:pianoVanilla}. This is the simplest architecture to translate the first piano voice to the second.
The input vector of size 100 is directly fed into a LSTM layer for every timeslice. As a hidden state dimension we use 100. The LSTM layer uses sigmoid activation to output a vector of 100 play probabilities for every note on the keyboard.
As a loss function we use cross-entropy loss. We decided against using a softmax function for the output, since the sum of our play probabilities can be larger than 1. Softmax normalises the sum over the output to 1.

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.7\textwidth]{figures/Piano_Vanilla.png}	
\end{center}
\caption{Piano Music - Vanilla Architecture}{
Simplest architecture: Input vector is always shown as a seperate 'Layer'. Dimensions are shown for clarification. None corresponds to inputs of arbitrary length. For example (None, None, 100) would mean an arbitrary batch size, arbitrary sequence length and 100 feature dimensions.  
}
\label{fig:pianoVanilla}
\end{figure}

\subsubsection{Results}	
As shown in \prettyref{fig:resvanilla}, the training causes a rapid improvement of the cross-entropy loss during the first 5 epochs, yet does not improve significantly afterwards. 

As shown in the prediction based on the goldberg dataset (middle panel), multiple tones are played in the second hand, which is expected due to the polyphonic character of piano music.
But the network is very unsure which of the tones should be played and there are only a few overlaps with the truly played notes. Also the temporal characteristics of the true second hand is not well matched at all by the prediction.

Because of the complex and ambiguous character of piano music and the interplay between left and right hand, many harmonic notes are possible, thus making the prediction hard. This results in a "smear" of tones around the notes of the first hand which might be harmonic to this voice, but are not completely harmonic towards each other, making the second voice sound inharmonic as a whole.
	
In the prediction of the second voice for the 'Ode to Joy' (lowest panel), the network predicts a second voice where many notes are played, but with a low probability. As expected, the model seems to understand that the second voice is generally lower than the first one. Like in the prediction for the goldberg dataset, the resulting second voice is not very harmonic, because small intervals (like two half-tones) are played at the same time.
	
An example audio file and the corresponding midi containing the prediction of the network on all goldberg variations is provided at \url{https://drive.google.com/open?id=0B5C_8h9Ht_tOSHdDX1k5OEtZZmc}, where the piano voice is the provided first hand and the strings are the predicted second hand. Using a piano for the second hand sounds unpleaseant. Because of the many notes with low play probability and the overall 'smeared' behaviour, it is also hard to tell when a note starts and ends. Using strings makes this easier, since starts and ends of notes do not sound so drastic.
	
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{figures/vanilla_RNN}\\
\includegraphics[width=0.8\textwidth]{figures/vanilla_rnn-bq_goldberg/goldberg-0.jpg}\\
\includegraphics[width=0.8\textwidth]{figures/vanilla_rnn-bq_goldberg/odetojoy-0.jpg}
	\mycaption{Training and prediction performance of the vanilla architecture}{From top to bottom: categorical cross-entropy loss during training, prediction on region of goldberg dataset and on Ode to Joy. Higher intensity corresponds to higher certainty of the network prediction.}
	\label{fig:resvanilla}
\end{figure}
	
\newpage 
	
\subsection{Deep Forward Backward LSTM}

As the obvious flaw of the vanilla architecture its limitation to be not aware of hidden states in the future. It processes the sequence solely in the direction of time.
This architecture is more sophisticated than the Vanilla network before. We feed the input sequence in two LSTM layers simultaneouly. The first goes through the sequence in correct order, the other one in reverse order.

The outputs of both are summed. This step is repeated one more time, making it a two layer network where both LSTM layers (having 2 LSTM neurons each) again have a hidden dimension of 100 like in the Vanilla Network before. The final output is again generated by a sigmoid activation and gives a play probability for every note on the keyboard over time.

A visualisation of the network structure is provided in \prettyref{fig:pianoDeep}.


\begin{figure}
\includegraphics[width=\textwidth]{figures/Piano_Bidir_Deep.png}
\caption{Piano Music - Deep Forward Backward Architecture}{
Input dimensions are shown as in the Vanilla Network before. The Merge layers use summation to combine LSTM outputs from forward and backward LSTM. In the end, a sigmoid activation function is applied. Cross-entropy loss is used for training.
}
\label{fig:pianoDeep}
\end{figure}

\subsubsection{Results}
Compared to the Vanilla architecture, this architecture shows a much lower loss during training and thus seems to be able to mimic the second voice much better (see \prettyref{fig:resdeep}). Also, in the prediction less notes are played at the same time, making the music created by the network sound clearer and also less inharmonic.

Although the network was trained with the second hand of piano music, the architecture is unable to produce a similar output to the training set. Most of the predictions include many notes that are held, and played over multiple bars, as it can be heared in the audio samples. Although these tones do not directly relate to the training dataset, the network seemed to have learned some basic concepts of harmony, as played tones are mostly harmonic to the first voice and only rarely cause inharmonic sound. Also is the base key of a chain of notes in the first voice changes, the prediction also changes and plays different notes.

Regarding the prediction based on the Ode to Joy, this architecture only rarely plays notes at all, probably this is due to the entirely different type of music compared to the goldberg variations.

An example audio file and the corresponding midi containing the prediction of the network on all goldberg variations is provided at \url{https://drive.google.com/open?id=0B5C_8h9Ht_tOSHdDX1k5OEtZZmc}, where the piano voice is the provided first hand and the strings are the predicted second hand.

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{figures/extended_deep_forward_backward}\\
\includegraphics[width=0.8\textwidth]{figures/extended_deep_forward_backward-bq_goldberg/goldberg-0.jpg}\\
\includegraphics[width=0.8\textwidth]{figures/extended_deep_forward_backward-bq_goldberg/odetojoy-0.jpg}
	\mycaption{Training and prediction performance of the deep forward backward architecture}{From top to bottom: categorical cross-entropy loss during training, prediction on region of goldberg dataset and on Ode to Joy. Higher intensity corresponds to higher certainty of the network prediction.}
	\label{fig:resdeep}
\end{figure}

\newpage 

\subsection{Key Invariant Transposing LSTM (KIT-LSTM)}
After results from the previous architectures indicated poor performance, we reasoned that the network might not be able to deal with different keys of the input. The main reason is that due to the formulation of the problem as a classification problem, the network does not see that the melody C-D-E-F-G is harmonically very similar to the transposed variant D-E-Fsharp-Gsharp-Asharp. The network can thus not generalize something it learned about harmony in the music in one key to all other keys.

In architecture shown in Figure \ref{fig:pianoKeyInvariant}, we try to make the internal analysis of temporal structure in the music key invariant.
It is composed of multiple parts with different intended responsibilities in the analysis.
The \textit{Pitch Analyser} is intended to determine the key of the provided as input. Through the subsequent L1 regularization, we try to force the network to produce a one-hot vector output of size 13.
By applying this output as a convolution filter on the input sequence, we hope to \textit{transpose} (in a musical sense) the input into a common key. Using a convolution mask to shift (transpose) data in a vector just works if this vector is really a one-hot vector.

Transposing the input will hopefully lead to a key-invariant analysis of harmony and temporal structure in the input.

The convolution was implemented using a custom \textit{TimeDistributed1DConvolution} layer, which applies the convolution mask to every time slice in a 1D fashion.

The central LSTM layer predicts the corresponding second voice in the transposed domain of the input. Thus a further convolution with the flipped filter is applied to transpose the output back into the key of the input.
As the convolutions are applied following the 'valid' rule (the filter is only applied to defined regions of the input data and is not padded during the convolution), the output of the Transpose-Prediction-Retranspose process is reduced by a total of $2 \times (filterlength - 1) = 14$ dimensions resulting in 76 output dimensions.

As the piano is not able to produce tones outside the midi pitch range of 21 to 108, and very high and low notes do not occur in the data, the tones that are lost during the convolution process should not influence our predictions. To compensate for this loss, the output of the retranspoition is padded with zeros. This had to be done using three separate layers (see Figure \ref{fig:pianoKeyInvariant}), as the Keras framework does not support padding on arbitrary axes.

\begin{figure}
\includegraphics[width=\textwidth]{figures/transpose_LSTM.png}
\caption{Piano Music - Key Invariant Architecture}{
The red box analyses the pitch of the input sequence. Its output is used to transpose (in a musical sense) the input sequence such that the LSTM layer (blue) can process the input in a common key. The green box perform zero padding to remove the artefacts introduced by the transposition step.
}
\label{fig:pianoKeyInvariant}
\end{figure}

\subsubsection{Results}
The training of this specially structured network was found to be especially tricky.
During the training runs, it was not possible to improve the predictions by further training and often the calculated loss became NaN, indicating exploding gradients.
Multiple approaches were followed to circumvent these errors (different regularisation, change of optimiser), yet lead to no improvement of the training.

The ability of the model to be trained seemed to strongly depend on the initialisation of the weights and often exploding gradients occurred after a number of training epoches. 
As this model is the most complex, the training took especially long, making it hard to improve the architecture and to cope with the training problems.

As the network could only be trained to a very low degree, no predictions of this network are shown. Overall, the general idea of making the input key independent could lead to a significant improvement of the model. Nevertheless, is was impossible to train this specially designed architecture.

	
\subsection{Summary}
All in all, the experiments show that prediction of a second piano voice based on a first given voice is a very hard problem. A few potential causes for the problems are explained in the last section of this project.














\newpage
	
\section{Prediction of 4th Voice in Bachs Chorales}
Due to the difficulties in the prediction of second voices in polyphonic music, we decided to dedicate ourselves to a more tractable problem, the prediction of a fourth choral voice.
Therefore we removed the highest voice of the 371 Bach Chorales dataset and tried to learn it from the remaining three. In the following section, we describe the two different architectures that were selected to tackle this problem.

\subsection{Architectural Choices}
In this context we decided to follow both a regression and a classification approach for the prediction of the highest voice.

In the case of a regression approach, the pitch is fed into the network as an integer number, thus tones that are in close proximity have a low difference.
In contrast to that, in an classification approach, every tone is independent of the others and no relation between them whatsoever is defined.

Architecturally, both networks are based on the previously explained forward-backward LSTM approach where the sequence is given to two independent networks in different order and the results are summed and passed to a final dense layer which creates a prediction either as a scalar (regression approach) or a one hot vector (classification approach).

The individual characteristics of the models are discussed in the following sections:

\subsection{4th Voice - Regression LSTM}
As shown in \prettyref{fig:choralRegression}, three integers go into the network for every timestep, and one scalar, denoting the pitch of the 4th voice is the output. This output has to be rounded to the nearest integer.


\begin{figure}[H]
\includegraphics[width=\textwidth]{figures/Chorales_ForBack.png}
\caption{Chorales 4th voice - Regression LSTM}{
This figure shows the architecture of the regression approach to learn the 4th voice in Bach Chorales. As input, we get three voices with a sequence length of 772 and 1 dimension containing the pitch. They are concatenated to have size 3 in the last dimension. A dense layer (one per timeslice) with linear activation then computes relative relationships between the pitches, resulting in a 10-dimensional vector over the whole sequence. This vector is fed into a forward and a backward LSTM layer with a hidden state size of 100. 
The results of the LSTM are concatenated together with the input voices (via a skip connection). A dense layer with a Relu activation function collapses the resulting 203 dimensions into 1 dimension, which will be the pitch of the 4th voice.
}
\label{fig:choralRegression}
\end{figure}

\subsubsection{Results}
The predictions of the model are shown in \prettyref{fig:choralRegressionProgress} at different stages of training. For this, we trained with 370 chorals, and predicting on the remaining one in every epoch to follow and visualise the learning progress.
Here, it is recognisable that the first training epochs solely shift the prediction upwards since this most easily reduces the mean square loss. After about ten epochs, the overall correct pitch is achieved, and further training finetunes the pitch locally to get closer to the real voice.

Overall, the network seems to provide acceptable performance when the plots are analysed, yet the acoustic representation of the tones are far from harmonic. Listening to the result, it is rather modern or 'jazzy', but not Bach-like.
One cause is probably the way how the loss is computed in the regression case. While for regular functions it is best to find predictions as close as possible to the original data, this approach fails in the context of music.
Here, tones that are far away can sound harmonic while tones that are very close to the original voice but do not exactly strike the right note sound very bad.

We will discuss this issue further in \prettyref{sec:ConclusionChorals}.

Examplary predictions based on two testing chorals are available online at \url{https://drive.google.com/open?id=0B5C_8h9Ht_tOSHdDX1k5OEtZZmc}, where the trombone voice corresponds to the predicted voice and the choir to the 3 other voices.

\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{music/choral0-epochbyepoch_regression/epoch0.jpg}
\includegraphics[width=0.5\textwidth]{music/choral0-epochbyepoch_regression/epoch1.jpg}\\
\includegraphics[width=0.5\textwidth]{music/choral0-epochbyepoch_regression/epoch10.jpg}\\
\includegraphics[width=0.5\textwidth]{music/choral0-epochbyepoch_regression/epoch49.jpg}
\end{center}
\caption{Chorales 4th Voice Regression - Learning Progress}{Learning process for the highest voice given the 3 other voices by regression approach. Epochs 0,1,10 and 49 are shown. Epoch 0 corresponds to random initialisation. The learned voice slowly approaches the pitch of the real voice.}
\label{fig:choralRegressionProgress}

\end{figure}
	
\subsection{4th Voice - Classification LSTM}
As shown in \prettyref{fig:choralClassification}, the model uses three one-hot vectors as input to the network network and outputs an one-hot vector as output. A softmax activation is used because we want only one note to be played at a time, and the softmax normalises the sum of the output play probabilities to one. To find out which note is played, we choose the one with the highest play probability.

\begin{figure}[H]
\includegraphics[width=\textwidth]{figures/Chorales_ForBack_Class.png}
\caption{Chorales 4th voice - Classification LSTM}{
This figure shows the architecture of the classification approach to learn the 4th voice in Bach Chorales. As input, we get three voices with a sequence length of 772 and 56 dimension containing one-hot vectors. They are summed such that they form a 'three-hot vector'. The result is fed into a forward and a backward LSTM layer with a hidden state size of 100. The output of both LSTMs is summed for every timeslice. The result is concatenated with the input voices via a skip-connection. A dense layer (per timeslice) with a softmax activation collapses the vector into 56 dimensions, one for each note of the output voice.}
\label{fig:choralClassification}
\end{figure}


\subsubsection{Results}
As shown in \prettyref{fig:choralClassificationProgress}, the model converges very fast from the initialisation to values close to the original voice. In contrast to the regression model, it does not require multiple epochs to shift the prediction to an appropriate pitch, but already predicts values in close proximity after one single epoch.

Also, after 10 epochs, the model already predicts the highest voice with much more precision than the regression network, as much fewer differences are recognisable. If the predictions are played back as music, they sound more harmonic compared to the regression approach.

One characteristic of the prediction is that the model sometimes misses the beginning of a bar, making the prediction sound out of rhythm rather than out of tune. This could be improved by further augmenting the data as explained in \prettyref{sec:ConclusionChorals}.

Examplary predictions based on two testing chorals are available online at \url{https://drive.google.com/open?id=0B5C_8h9Ht_tOSHdDX1k5OEtZZmc}, where the trombone voice corresponds to the predicted voice.

\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{music/choral0-epochbyepoch_classification/epoch0.jpg}\\
\includegraphics[width=0.5\textwidth]{music/choral0-epochbyepoch_classification/epoch1.jpg}\\
\includegraphics[width=0.5\textwidth]{music/choral0-epochbyepoch_classification/epoch10.jpg}\\
\includegraphics[width=0.5\textwidth]{music/choral0-epochbyepoch_classification/epoch49.jpg}

\end{center}
\mycaption{Chorales 4th Voice Classification - Learning Progress}{Learning process for the highest voice given the 3 other voices by classification approach. Epochs 0,1,10 and 49 are shown. After one epoch, the architecture learnes to play mostly C (pitch 60), which is never totally wrong for music in C major. After more and more epochs, the learned voice approaches the real one with remarkable precision.}
\label{fig:choralClassificationProgress}
\end{figure}

