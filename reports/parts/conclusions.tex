\chapter{Conclusions}
\section{Polyphonic Piano Voice}
The experiments with piano music showed that the prediction of a second voice solely based on a first voice is a very hard problem. The networks were unable to reproduce a second voice that sounds similar to the original training dataset. We think the temporal and harmonical structure of the music was to complicated to be learned by our rather simple architectures.

For further evaluation of the required network structure a scan of hyperparameters could have been applied. This approach was not followed due to the high requirements on computational power, which would have been required to compare many network structures.

Furthermore, an easier dataset, where the training examples are always written in the same key could significantly improve the performance of the network. The Goldberg variations we used as training data are written in many different keys. This further increases the difficulties for our architecture, since in a classification approach, the notes are independent. That means, two nearby pitches are as similar for the model as two very far apart pitches. The same piece of music transposed to another key is a completely different input, even though certain notes can be treated very similarly by harmonic means (e.g. one octave intervals). Thus, the model cannot infer general rules about e.g. harmony, that are the same in every key.
A future possibility is to musically transpose all training examples based on an automatic key analysis or annotated keys using for example the \textit{music21} library.

We tried to incorporate this key analysis directly into our network but failed due to problems in training. There are further problems with our Key Invariant Transposing LSTM. When the infered convolution mask is not a one-hot vector, the transposition by it will fail. We hope that the weights in the network adjust accordingly so that the transposition step works, since this will likely lead to the best performance and the lowest loss. Further work on this more complicated architecture would be interesting to see if this actually works.

\subsection{Ambiguity}

The difference between the second voice used in training and the prediction of the trained network is probably due to the polyphonic character of the music and the resulting ambiguity of predictions. If during training the network learns multiple examples where a similar first hand leads to very different second hands, it cannot with high certainty be able to determine the correct second hand.

Of course, ideally the recurrent network should be able to deduce the correct notes by the context, allowing only a small subset of possible pitches. As seen in the experiments, the more complex forward backward network plays only a few notes compared to the vanilla network which plays many notes at the same time. It is possible, that the more complex network is thus able to better determine these restrictions due to the context reducing the ambiguity of the prediction.







\section{Bach Chorals}
\label{sec:ConclusionChorals}
The regression approach was problematic, leading to the new voice being off by small intervals from the correct voice. The sound is quite interesting and very modern, but it failed to mimic the way Bach composed his chorals. 

\subsection{Musical Loss Function instead of Mean Squared Error}

The problem with the regression model for finding the 4th voice in Bach Chorales is that we are using a mean squared error (MSE) objective function. From a musical standpoint, this is problematic, because the feeling of harmony in music is not described by that function. Since music composition is very ambiguous, there are many ways to write a 4th voice in the chorales. The MSE will thus never be zero. So on average, we might be a halftone off the right solution. By standards of the MSE Loss, this is not a bad solution, but from a harmonic standpoint, this is as wrong as it could be. Intervals of one half-tone should therefore be punished very hard, but intervals of maybe 5 half-tones, corresponding to a perfect forth, should be punished less. 

\begin{figure}
\includegraphics[width=\textwidth]{figures/musical_loss.png}
\caption{Suggestion of a Musical Loss Function}{
(1) shows a histogram of intervals between all pairs of voices in the 371 Bach Chorales dataset for every timestep. If at least one voice is not singing, the timestep is excluded from the computation. Intervals of a voice with itself are always excluded. The zero intervals result from voices singing at the same pitch.
(2) suggestion of a 'musical loss function'. The loss at interval zero is set to zero, so if the model predicts the correct pitch, there will be no loss. The loss at the other points is computed according to the heuristic $Loss = 0.5 + 3 * \frac{1/counts}{max(1/counts)}$.
}
\label{fig:musicalLossFunction}
\end{figure}

To get an idea how a harmonically optimised Loss function would look like, we analysed the intervals between the voices in the 371 Bach Chorales Dataset. \prettyref{fig:musicalLossFunction} shows the corresponding histogram and a heuristic how intervals could be punished by a Loss function.

The authors are well aware that there are problems with this kind of Loss function, like local minima far away from the correct solution. Future work might find a solution for this problem to help find harmonically correct melodies by regression approaches.

Compared to the regression approach, inference of the 4th voice in Bach chorales by classification was highly successful. The notes are treated independently there, so for the model, being a half-tone off is as wrong as being several octaves off.

\subsection{Include rhythm information to input X}

Listening to the predicted choral voices, we found that the model is very good in predicting the correct pitch, but in the time axis, the prediction is sometimes a bit off. The model has no real feeling for rythm, it just gets a stream of timeslices. The input data has no information about when a measure starts and ends.
We suggest that we can append the input vector with a representation of rythm within a measure. E.g. we can append a line containing (1,0,0,0,1,0,0,0...) telling the model at which point in the measure it is right now. This will likely improve the accuracy of melody lines in the time dimension.

\section{Outlook}

In the project we learned to work with the high-level framework Keras and adjust it to our needs. A quite significant amount of time was spent on generating and cleaning the required training data. 

While we failed to compose realistic second voices for polyphonic piano music, we succeeded on a more tractable problem: composing the highest voice in Bach chorals (all in C major) using our classification approach with a custom architecture implemented in Keras.

There are remaining questions from this project, that can also be interesting for people studying the features of music. Since we can infer the highest voice in Bach chorals pretty well, is this also the case when we try to infer one of the other voices? What can we learn about music composition when we look at our trained model? The models wisdom is saved in a big number of weights, but we could run it on artificial test data and look how the prediction changes with changes in the data.





