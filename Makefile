.PHONY: clean data lint requirements sync_data_to_s3 sync_data_from_s3

#################################################################################
# GLOBALS                                                                       #
#################################################################################

ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
pythonpath := $(ROOT_DIR)

# pass parameters of make to function

ifneq (,$(filter $(firstword $(MAKECMDGOALS)),training prediction))
  # use the rest as arguments
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

datafile :=

#################################################################################
# COMMANDS                                                                      #
#################################################################################

requirements: .requirements

.requirements:
	pip install -q -r requirements.txt
	@touch .requirements

data: .data

.data: .requirements
	PYTHONPATH=$(pythonpath) python src/data/datasets.py
	@touch .data

notebook: requirements
	PYTHONPATH=$(pythonpath) jupyter notebook

training: data
	PYTHONPATH=$(pythonpath) python src/model/training.py $(RUN_ARGS)

prediction: data
	PYTHONPATH=$(pythonpath) python src/model/prediction.py $(RUN_ARGS)

clean:
	find . -name "*.pyc" -exec rm {} \;

lint:
	flake8 --exclude=lib/,bin/ .


#################################################################################
# PROJECT RULES                                                                 #
#################################################################################
