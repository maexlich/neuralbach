import numpy as np
import itertools
import matplotlib
matplotlib.use('Agg')
from os import path
from src.data.datasets import do_dataset_action
from src.visualization.visualize import plot_datamatrix, plot_two_voices, plot_chorals_with_prediction, chorals_to_music21
import matplotlib.pyplot as plt


def predict_on_dataset(datasetname, model, chunksize=None):
    """
    Execute prediction on a dataset.

    Args:
        datasetname: name of dataset
        model: the model that shall be used to predict
        chunksize: optional length cutoff to not predict on all data

    Returns:
        :class:`tuple`: three element tuple containing X, y, and prediction
    """
    result = do_dataset_action(datasetname, "get_testing")
    X, y = result if isinstance(result, tuple) else (result, None)

    if chunksize:
        predicted = model.predict(X[:, :chunksize])
        if y is not None:
            y = y[:, :chunksize]
    else:
        predicted = model.predict(X)
    return X, y, predicted


def save_or_show_plot(datasetname, suffix, outputpath=None):
    if outputpath:
        from os import path, makedirs
        if not path.exists(outputpath):
            makedirs(outputpath, exist_ok=True)
        filename = "{datasetname}-{suffix}.pdf".format(
            datasetname=datasetname, suffix=suffix)
        plt.savefig(path.join(outputpath, filename))
    else:
        plt.show()


def predict_piano_on_test(predictionname, model, dataset,
                          n_plots=5, chunksize=256, outputpath=None):
    X, y, predicted = predict_on_dataset(dataset, model, chunksize=chunksize)
    # do for every batch predicted
    for i in range(min(n_plots, X.shape[0])):
        fig = plt.figure()
        red = (1, 0, 0)
        green = (0, 1, 0)
        blue = (0, 0, 1)
        title = '1st voice (red), 2nd voice prediction (green)'
        ax = plot_two_voices(X[i], predicted[i], red, green)
        if y is not None:
            plot_datamatrix(y[i], note_color=blue, ax=ax)
            title += ', 2nd voice (blue)'
        fig.suptitle(title)
        save_or_show_plot(predictionname, str(i), outputpath=outputpath)
        plt.close(fig)


def predict_piano_on_goldberg(predictionname, model, outputpath):
    predict_piano_on_test(predictionname, model,
                          "goldberg", outputpath=outputpath)
    predict_music_from_midi('data/interim/goldberg.mid',
                            model, path.join(outputpath, predictionname + ".mid"))


def predict_music_from_midi(midifile, model, outputpath):
    from mido import MidiFile
    from src.data.midi2matrix import track2recarray, recarray2datamatrix
    from src.data.matrix2midi import datamatrix2stringtrack, make_midi_from_tracks
    track1 = MidiFile(midifile).tracks[1]
    _, data1 = recarray2datamatrix(track2recarray(track1))
    # TODO: Something wrong with dimensions
    prediction = model.predict(np.array([data1]))
    track2 = datamatrix2stringtrack(prediction)
    make_midi_from_tracks([track1, track2], outputpath)


def predict_piano_on_sonata(predictionname, model, outputpath):
    predict_piano_on_test(predictionname, model,
                          "beethoven_sonata", outputpath=outputpath)


def generate_random_voice(randomrange, chunksize):
    output = np.zeros((chunksize, 100))
    # for every quarter sample a tone
    notes = np.random.randint(*randomrange, chunksize // 8)
    for pos in range(0, chunksize // 8):
        output[pos * 8:pos * 8 + 8, notes[pos]] = 1

    return output.astype(bool)


def predict_piano_on_random(predictionname, model, randomrange=(
        40, 80), chunksize=256, outputpath=None):
    # If model has defined input shape, use that as chunksize
    if model.input_shape[1]:
        chunksize = model.input_shape[1]

    randomdata = generate_random_voice(randomrange, chunksize)
    prediction = model.predict(np.array([randomdata]))

    fig = plt.figure()
    plot_two_voices(randomdata, prediction[0], (1, 0, 0), (0, 1, 0))
    fig.suptitle(
        "Predictions (green) on randomly generated data (red, range {}-{})".format(*randomrange))

    save_or_show_plot(predictionname, chunksize, outputpath=outputpath)
    plt.close(fig)


def predict_piano_tone_invariance(
        predictionname, model, dataset='goldberg', chunksize=256, shift_by=-5, outputpath=None,):
    X, y, prediction = predict_on_dataset(dataset, model, chunksize=chunksize)
    shiftedX = X + shift_by
    shifted_prediction = model.predict(shiftedX)

    fig = plt.figure()
    plot_two_voices(prediction[0], shifted_prediction[0], (1, 0, 0), (0, 1, 0))
    fig.suptitle("Second voice prediction on unshifted {} dataset (red) and shifted by {} halftones (green)".format(
        dataset, shift_by))
    save_or_show_plot(predictionname, chunksize, outputpath=outputpath)
    plt.close(fig)


def predict_on_odetojoy(predictionname, model,
                        chunksize=None, outputpath=None):
    predict_piano_on_test(predictionname, model, "odetojoy",
                          chunksize=chunksize, outputpath=outputpath)


def predict_regression_on_chorals(
        predictionname, model, max_plots=5, outputpath=None):
    X, y = do_dataset_action("bach_chorals_regression", 'get_testing')
    original = np.swapaxes(np.concatenate([y] + X, axis=-1), -1, -2)
    predictions = model.predict(X)
    for index in range(min(predictions.shape[0], max_plots)):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plot_chorals_with_prediction(original[index], predictions[index])
        save_or_show_plot(predictionname, index + 1, outputpath=outputpath)
        plt.close(fig)
        create_midi_from_choral(
            index + 1, original[index, 1:], predictions[index, :, 0][np.newaxis, :], outputpath)


def predict_classification_on_chorals(
        predictionname, model, max_plots=5, outputpath=None):
    X, y, predictions = predict_on_dataset(
        "bach_chorals_classification", model)
    prediction_reg = np.argmax(predictions, axis=-1)
    X_reg, y_reg = do_dataset_action("bach_chorals_regression", 'get_testing')

    original = np.swapaxes(np.concatenate([y_reg] + X_reg, axis=-1), -1, -2)
    for index in range(min(predictions.shape[0], max_plots)):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plot_chorals_with_prediction(original[index], prediction_reg[index])
        save_or_show_plot(predictionname, index + 1, outputpath=outputpath)
        plt.close(fig)
        create_midi_from_choral(
            index + 1, original[index, 1:], prediction_reg[index][np.newaxis, :], outputpath)


def create_midi_from_choral(suffix, X, prediction, outputpath):
    concatinated = np.concatenate([prediction, X], axis=0)
    filename = "{datasetname}-{suffix}.mid".format(
        datasetname="bach_choral", suffix=suffix)
    from os import path
    chorals_to_music21(concatinated, output=path.join(outputpath, filename))

predictions = {
    # Piano datasets
    "random_data": predict_piano_on_random,
    "tone_invariance": predict_piano_tone_invariance,
    "goldberg": predict_piano_on_goldberg,
    "beethoven_sonata": predict_piano_on_sonata,
    "odetojoy": predict_on_odetojoy,
    "bach_choral_regression": predict_regression_on_chorals,
    "bach_choral_classification": predict_classification_on_chorals
}


def get_prediction_names():
    return [name for name, call in predictions.items()]


def get_parser():
    import argparse
    from src.model.models import get_model_types
    parser = argparse.ArgumentParser(
        description='Predict on test datasets using a model')
    parser.add_argument("modelname", action='store', choices=[choise for choise in get_model_types().keys()],
                        help='Name of model to predict with')
    parser.add_argument("trainingname", type=str, help='Name of training run')
    parser.add_argument("predictions", nargs='+', action='append', choices=get_prediction_names(),
                        help='Predictions to perform.')
    # parser.add_argument("chunksize", type=int, help='Size of chunk that should be used for prediction')
    parser.add_argument("--output-suffix", dest='output_suffix', type=str, default='',
                        help='Append following string to the output path')
    parser.add_argument("--override-path", dest='override_path', type=str, default=None,
                        help='Override default output path with provided string. If not provided results are stored under report/figures')

    return parser


def main(*arguments):
    myparser = get_parser()
    args = myparser.parse_args(args=arguments)
    selected_predictions = list(itertools.chain.from_iterable(args.predictions))
    from src.model.generic import load_model
    model = load_model(args.modelname, args.trainingname)

    if args.override_path:
        path = args.override_path
    else:
        path = "reports/figures/{}-{}{}".format(
            args.modelname, args.trainingname, args.output_suffix)

    for prediction in selected_predictions:
        predictions[prediction](prediction, model, outputpath=path)

if __name__ == "__main__":
    from sys import argv
    main(*argv[1:])
