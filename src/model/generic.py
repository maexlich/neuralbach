import os.path
from os import makedirs
from keras.models import model_from_json
from src.model.custom_layers import TimeDistributed1DConvolution

filepattern = "{modeltype}-{subtype}"


def get_model_path(modeltype, subtype, basepath="models"):
    target = filepattern.format(modeltype=modeltype, subtype=subtype)
    target = os.path.join(basepath, target)
    return target


def save_model(modeltype, subtype, model, basepath="models",
               weights_only=False, structure_only=False):
    """Save model into basepath in defined structure.

    Model definition: {modeltype}-{subtype}.json
    Model weights: {modeltype}-{subtype}.h5

    Args:
        modeltype: The type of the model as defined in :func:`src.model.models.get_model`
        subtype: subtype describing special parameters
        model: :class:`keras.models.Model` that should be stored
        basepath (str, optional): basepath of operations
        weights_only (bool, optional): only save weights
        structure_only (bool, optional): only save structure as json

    """
    if structure_only and weights_only:
        raise Exception("That is impossible...")

    target = get_model_path(modeltype, subtype, basepath=basepath)

    if len(target.split(os.path.sep)) > 1:
        makedirs(os.path.dirname(target), exist_ok=True)
    if not weights_only:
        with open(target + ".json", "w") as model_file:
            model_file.write(model.to_json())
    if not structure_only:
        model.save_weights(target + ".h5", overwrite=True)


def load_model(modeltype, subtype, basepath="models"):
    """Loads a model and weights from defined structure.

    See :func:`src.model.generic.save_model` for further details

    Args:
        modeltype (str): The type of the model as defined in :func:`src.model.models.get_model`
        subtype (str): subtype describing special parameters
        basepath (str, optional): basepath of operations

    Returns:
        :class:`keras.models.Model`: The loaded model
    """
    target = get_model_path(modeltype, subtype, basepath=basepath)

    model = model_from_json(open(target + ".json", "r").read(), custom_objects={
                            "TimeDistributed1DConvolution": TimeDistributed1DConvolution})
    model.load_weights(target + ".h5")

    return model
