from keras.models import Sequential, Model
from keras.layers import LSTM, merge, Input, Permute, Dense, ZeroPadding1D, ActivityRegularization, Activation, TimeDistributed, Lambda, Dropout
from src.model.custom_layers import TimeDistributed1DConvolution
from keras.regularizers import l1


def get_model(modeltype, parameters={}, compile_parameters={}):
    """Get a model of defined type with parameters and compile parameters.

    Args:
        modeltype (str): gets a model type defined by :func:`src.model.models.get_model_types`
        parameters (:class:`dict`): parameters to pass model creation (dependent of model)
        compile_parameters (:class:`dict`): parameters to pass during model compilation

    Returns:
        :class:`keras.model.sequential`: the requested model
    """
    modeltypes = get_model_types()
    model = modeltypes[modeltype](compile_parameters, **parameters)

    return model


def get_model_types():
    """Get model types.

    Returns:
        :class:`dict`: dictionary of modelnames as keys and creation functions.
    """
    return {
        "vanilla_rnn": get_vanilla,
        "deep_forward_backward": get_deep_forward_backward,
        "extended_deep_forward_backward": extended_deep_forward_backward,
        "convolutional": get_convolutional,
        "chorals_regression": get_vanilla_choral_regression,
        "chorals_classification": get_vanilla_choral_classification
    }


def get_vanilla(compile_parameters, **layer_parameters):
    """Vanilla LSTM RNN for prediction of polyphonic piano music.

    Maps a (t,100)-dimensional input sequence of length t to the (t,100)-dimensional output
    sequence of length t.

    Args:
        compile_parameters: Additional parameters for compilation
        **params: additional parameters for layer creation
            defaults:
                * output_dim, input_dim
                * return_sequences
                * activation: hard_sigmoid
                * inner_activation: hard_sigmoid

    Returns:
        :class:`keras.models.sequential`: The Vanilla RNN

    """
    default_layer = {
        "output_dim": 100,
        "input_dim": 100,
        "return_sequences": True,
        "activation": 'hard_sigmoid',
        'inner_activation': 'hard_sigmoid'
    }
    default_compile = {
        "optimizer": 'rmsprop',
        "loss": 'categorical_crossentropy',
        "metrics": ['accuracy']
    }

    default_layer.update(layer_parameters)
    default_compile.update(compile_parameters)

    model = Sequential()
    model.add(LSTM(**default_layer, name='LSTM-Layer'))

    model.compile(**default_compile)

    return model


def get_deep_forward_backward(compile_parameters, **params):
    """Deep Forward Backward LSTM RNN for prediction of polyphonic piano music.

    Maps a (t,100)-dimensional input sequence of length t to the (t,100)-dimensional output
    sequence of length t.
    Uses intermediate LSTM RNN layers to model more complex relationships between the 2 sequences.

    Args:
        compile_parameters: Additional parameters for compilation
        **params: additional parameters for layer creation
            defaults:
                * hidden_dims: list of number of dimensions in hidden layers, default: [100,100]
                * merge_mode: How to merge forward and backward LSTM RNN layers. default: 'sum'
                * dropout: 0.05

    Returns:
        :class:`keras.models.sequential`: The deep forward backward network RNN

    """
    default_params = {
        "hidden_dims": [100, 100],
        "merge_mode": 'sum',
        "dropout": 0.05
    }
    default_compile = {
        "optimizer": 'rmsprop',
        "loss": 'categorical_crossentropy',
        "metrics": ['accuracy']
    }

    default_params.update(params)
    default_compile.update(compile_parameters)

    input_sequence = Input(shape=(None, 100), name='Input Sequence')  # input dim == output dim
    previous_layer = input_sequence
    all_dims = [100] + default_params['hidden_dims']

    for i, layer_size in enumerate(default_params["hidden_dims"]):
        rnn_fwd = LSTM(layer_size, return_sequences=True,
                       name='LSTM_Forward_%s' % (i + 1))(previous_layer)

        rnn_bwd = Sequential()
        rnn_bwd.add(LSTM(layer_size, return_sequences=True, go_backwards=True, input_shape=(None, all_dims[i])))
        rnn_bwd.add(Lambda(lambda x: x[:, ::-1, :], output_shape=lambda x: x))
        rnn_bwd.name = 'LSTM_Backward_%s' % (i + 1)
        rnn_bwd_rev = rnn_bwd(previous_layer)

        merged = merge([rnn_fwd, rnn_bwd_rev], mode=default_params[
                               'merge_mode'], name='LSTM_Bidirectional_%s' % (i + 1))
        previous_layer = Dropout(default_params['dropout'])(merged)

    output = Activation(activation='sigmoid',
                        name='Sigmoid Activation')(previous_layer)
    model = Model(input=input_sequence, output=output)

    model.compile(**default_compile)
    return model

def extended_deep_forward_backward(compile_parameters, **params):
    return get_deep_forward_backward({}, hidden_dims=[400, 100])


def get_convolutional(compile_parameters, **params):
    """LSTM RNN with convolutional layer before and after the RNN.

    The hypothesis is that it transposes the input
    such that in the intermediate RNN all music pieces have the same pitch.
    Maps a (t,100)-dimensional input sequence of length t to the (t,100)-dimensional output
    sequence of length t.

    Args:
        compile_parameters: Additional parameters for compilation
        **params: additional parameters for layer creation
            defaults:
                * size_convolution_mask
                * reg_convolution_mask: regularisation of convolution mask
                * transpose_activation: activation function to use after transpose (default: softmax)

    Returns:
        :class:`keras.models.sequential`: The convolutional RNN

    """
    default_params = {
        "size_convolution_mask": 13,
        "reg_convolution_mask": 0.01,
        "transpose_activation": 'softmax'
    }

    default_compile = {
        "optimizer": 'rmsprop',
        "loss": 'categorical_crossentropy',
        "metrics": ['accuracy']
    }
    default_params.update(params)
    default_compile.update(compile_parameters)

    input_sequence = Input(shape=(None, 100), name='input_sequence')

    # learns how to transpose with convolution mask
    transpose_mask = LSTM(
        default_params["size_convolution_mask"], activation='relu')(input_sequence)
    transpose_mask_reg = ActivityRegularization(l1=default_params["reg_convolution_mask"],
                                                name='transpose_mask_reg')(transpose_mask)
    transpose_mask_reg_softmax = Activation(
        default_params['transpose_activation'])(transpose_mask_reg)

    # transpose input sequence

    transposed = TimeDistributed1DConvolution()(
        [input_sequence, transpose_mask_reg_softmax])

    output_sequence = LSTM(88, return_sequences=True,
                           activation='sigmoid', inner_activation='hard_sigmoid')(transposed)

    retransposed = TimeDistributed1DConvolution(flip_filter=True)(
        [output_sequence, transpose_mask_reg_softmax])

    # retransposed = merge([output_sequence, transpose_mask_reg_softmax],
    #                      mode=transpose_reverse, output_shape=(None, 176))

    # pad it to 200 dimensions again
    perm = Permute((2, 1))(retransposed)
    padded = ZeroPadding1D(12)(perm)
    reperm = Permute((2, 1))(padded)

    model = Model(input=input_sequence, output=reperm)

    model.compile(**default_compile)

    return model


def get_vanilla_choral_regression(compile_parameters, **params):
    """Vanilla LSTM RNN for chorals based regression.

    Maps a t-dimensional input sequence of length t to the t-dimensional output
    sequence of length t.

    Args:
        compile_parameters: Additional parameters for compilation
        **params: additional parameters for layer creation
            defaults:
                * data_sequence_length: length of input sequences
                * LSTM_layer_size: hidden dim of lstm layer

    Returns:
        :class:`keras.models.sequential`: The vanilla RNN for choral regression

    """

    default_params = {
        "data_sequence_length": 772,
        "LSTM_layer_size": 100
    }

    default_compile = {
        "optimizer": 'rmsprop',
        "loss": 'mse',
        "metrics": ['accuracy']
    }
    default_params.update(params)
    default_compile.update(compile_parameters)

    voice1 = Input(shape=(
        default_params["data_sequence_length"], 1), dtype='float32', name='Voice1')
    voice2 = Input(shape=(
        default_params["data_sequence_length"], 1), dtype='float32', name='Voice2')
    voice3 = Input(shape=(
        default_params["data_sequence_length"], 1), dtype='float32', name='Voice3')

    merged = merge([voice1, voice2, voice3], mode='concat',
                   concat_axis=-1, name='Voice1,2,3')

    dense1 = TimeDistributed(
        Dense(10, activation='linear', bias=True, W_regularizer=l1(0.01)), name='DenseOverTime - Linear')(merged)

    rnn_fwd = LSTM(default_params["LSTM_layer_size"], return_sequences=True,
                   activation='hard_sigmoid', name='LSTM_Forward')(dense1)

    rnn_bwd = Sequential()
    rnn_bwd.add(LSTM(default_params["LSTM_layer_size"], return_sequences=True,
                     activation='hard_sigmoid', go_backwards=True, input_shape=(default_params["data_sequence_length"], 10)))
    rnn_bwd.add(Lambda(lambda x: x[:, ::-1, :]))
    rnn_bwd.name = 'LSTM_Backward'
    rnn_bwd_rev = rnn_bwd(dense1)

    rnn_merge = merge([rnn_fwd, rnn_bwd_rev], mode='sum',
                      concat_axis=-1, name='LSTM-Output')

    merged_rnn_input = merge([merged, rnn_merge], mode='concat',
                             concat_axis=-1, name='Voice1,2,3 + LSTM-Output')

    dense2 = TimeDistributed(Dense(1, activation='relu', bias=True, W_regularizer=l1(0.01)),
                             name='Relu -- Voice4')(merged_rnn_input)

    model = Model(input=[voice1, voice2, voice3], output=dense2)
    model.compile(**default_compile)

    return model


def get_vanilla_choral_classification(compile_parameters, **params):
    """Vanilla LSTM RNN for chorals based classification.

    Maps a t-dimensional input sequence of length t to the t-dimensional output
    sequence of length t.

    Args:
        compile_parameters: Additional parameters for compilation
        **params: additional parameters for layer creation
            defaults:
                * data_sequence_length: length of input sequences
                * LSTM_layer_size: hidden dim of lstm layer

    Returns:
        :class:`keras.models.sequential`: The vanilla RNN for choral classification

    """
    default_params = {
        "data_sequence_length": 772,
        "LSTM_layer_size": 100
    }
    default_compile = {
        'optimizer': 'rmsprop',
        'loss': 'categorical_crossentropy',
        'metrics': ['accuracy']
    }
    default_params.update(params)
    default_compile.update(compile_parameters)

    voice1 = Input(shape=(
        default_params["data_sequence_length"], 56), dtype='float32', name='Voice1')
    voice2 = Input(shape=(
        default_params["data_sequence_length"], 56), dtype='float32', name='Voice2')
    voice3 = Input(shape=(
        default_params["data_sequence_length"], 56), dtype='float32', name='Voice3')

    merged = merge([voice1, voice2, voice3], mode='sum',
                   concat_axis=-1, name='Voice1,2,3')

    rnn_fwd = LSTM(default_params["LSTM_layer_size"], return_sequences=True,
                   activation='tanh', name='LSTM_Forward')(merged)

    rnn_bwd = Sequential()
    rnn_bwd.add(LSTM(default_params["LSTM_layer_size"], return_sequences=True,
                     activation='tanh', go_backwards=True, input_shape=(default_params["data_sequence_length"], 56)))
    rnn_bwd.add(Lambda(lambda x: x[:, ::-1, :]))
    rnn_bwd.name = 'LSTM_Backward'
    rnn_bwd_rev = rnn_bwd(merged)

    rnn_merge = merge([rnn_fwd, rnn_bwd_rev], mode='sum',
                      concat_axis=-1, name='LSTM-Output')

    merged_rnn_input = merge([merged, rnn_merge], mode='concat',
                             concat_axis=-1, name='Voice1,2,3 + LSTM-Output')

    dense2 = TimeDistributed(Dense(56, activation='softmax', bias=True, W_regularizer=l1(0.00)),
                             name='Softmax---Voice4')(merged_rnn_input)

    model = Model(input=[voice1, voice2, voice3], output=dense2)
    model.compile(**default_compile)

    return model


if __name__ == "__main__":
    for name in get_model_types():
        _ = get_model(name)
