import numpy as np
import csv
from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping
from src.data.datasets import do_dataset_action, get_dataset_names
from src.model.models import get_model_types, get_model
from src.model.generic import save_model, get_model_path, load_model


class CsvLogCallback(Callback):
    """CSV-logger callback for keras training.

    Stores the loss ant defined metrics of training into file for every epoch.

    """

    def __init__(self, logfile, existing=False, dialect='unix'):
        self.logfile = logfile
        self.dialect = dialect
        if existing:
            with open(logfile, 'r') as file:
                reader = csv.reader(file, dialect=dialect)
                self.order = next(reader)
                self.order.remove("epoch")
            self.wrote_header = True
        else:
            self.wrote_header = False

    def write_header(self, log):
        self.order = []
        for key, value in log.items():
            self.order.append(key)

        with open(self.logfile, 'w') as file:
            csvwriter = csv.writer(
                file, dialect=self.dialect, quoting=csv.QUOTE_MINIMAL)
            csvwriter.writerow(["epoch"] + self.order)

        self.wrote_header = True

    def on_epoch_end(self, epoch, logs={}):
        if not self.wrote_header:
            self.write_header(logs)

        with open(self.logfile, "a") as file:
            csvwriter = csv.writer(
                file, dialect=self.dialect, quoting=csv.QUOTE_MINIMAL)
            data = [epoch + 1]
            for field in self.order:
                data.append(logs[field])

            csvwriter.writerow(data)


class PredictionCallback(Callback):
    """Prediction Callback for keras training.

    Execute predictions during training.

    """

    def __init__(self, modelname, trainingname, predictions, steps):
        self.modelname = modelname
        self.trainingname = trainingname
        self.predictions = predictions
        self.steps = steps

    def on_epoch_end(self, epoch, logs={}):
        if epoch % self.steps == 0:
            print("Storing predictions for epoch ", epoch + 1)
            from src.model.prediction import main
            main(self.modelname, self.trainingname, *self.predictions,
                 "--output-suffix", "-{}".format(epoch + 1))


def combine_multi_datasets(datasets):
    """
    Combines datasets that either provide multiple inputs or outputs
    Args:
        datasets: list of datasets that themselves contain lists of inputs/outputs

    Returns:
        tuple:
        Tuple of the inputs/outputs of a dataset concatenated along the batch dimension
    """
    # stack the inputs along a new dimension (axis=0) and concat over batch
    # dimension (axis=1)
    concatinated = np.concatenate(
        [np.stack(dataset, axis=0) for dataset in datasets], axis=1)
    # then split back into inputs along the input dimension and remove extra
    # axis using squeeze
    combinded_data = [np.squeeze(input, axis=0) for input in np.split(
        concatinated, concatinated.shape[0], axis=0)]
    return combinded_data


def get_parser():
    import argparse
    from src.model.prediction import get_prediction_names
    parser = argparse.ArgumentParser(
        description='Train a model and store training progress')
    parser.add_argument("modelname", action='store', choices=[choise for choise in get_model_types().keys()],
                        help='Name of model to train')
    parser.add_argument("trainingname", type=str, help='Name of training run')
    parser.add_argument("epochs", type=int, help='Nr of epoches to train')
    parser.add_argument("datasets", nargs='+', action='append', choices=get_dataset_names(),
                        help='dataset to use for training')
    parser.add_argument("--continue-training", "-c", dest='continue_training', action='store_true', default=False,
                        help='Continue existing training (default: %(default)s)')
    parser.add_argument("--batch-size", dest='batch_size', type=int, default=10,
                        help='Batch size for training (default: %(default)s)')
    parser.add_argument("--predictions", dest="predictions", action='append', nargs="+", choices=get_prediction_names(),
                        help="Predict after each epoch")
    parser.add_argument("--save-steps", dest='save_steps', type=int, default=1,
                        help='Only do prediction every nth epoch (default: %(default)s)')
    parser.add_argument("--patience", type=int, default=8,
                        help='Stop training after this number of epochs did not yield improvement (default: %(default)s)')

    # parser.add_argument("--predictions", nargs='*', action='append'
    # choices=get_prediction_names(), help='Do these pre')

    parser.set_defaults(predictions=[])

    return parser


if __name__ == '__main__':
    myparser = get_parser()
    args = myparser.parse_args()

    print("Training model: {}-{}".format(args.modelname, args.trainingname))
    import itertools

    datasets = list(itertools.chain.from_iterable(args.datasets))
    predictions = list(itertools.chain.from_iterable(args.predictions))
    # predictions = args.predictions
    path = get_model_path(args.modelname, args.trainingname)

    training_X = []
    training_Y = []

    testing_X = []
    testing_Y = []

    for dataset in datasets:
        Xtrain, Ytrain = do_dataset_action(dataset, "get_training")
        training_X.append(Xtrain)
        training_Y.append(Ytrain)
        Xtest, Ytest = do_dataset_action(dataset, "get_testing")
        testing_X.append(Xtest)
        testing_Y.append(Ytest)

    # only one input
    if isinstance(training_X[0], np.ndarray):
        training_X = np.concatenate(training_X)
        testing_X = np.concatenate(testing_X)
    # multiple inputs
    else:
        training_X = combine_multi_datasets(training_X)
        testing_X = combine_multi_datasets(testing_X)

    # only one output
    if isinstance(training_Y[0], np.ndarray):
        training_Y = np.concatenate(training_Y)
        testing_Y = np.concatenate(testing_Y)
    # multiple outputs
    else:
        training_Y = combine_multi_datasets(training_Y)
        testing_Y = combine_multi_datasets(testing_Y)

    model = get_model(args.modelname)

    if args.continue_training:
        model = load_model(args.modelname, args.trainingname)

    save_model(args.modelname, args.trainingname, model, structure_only=True)

    callbacks = [
        CsvLogCallback(path + ".csv", existing=args.continue_training),
        ModelCheckpoint(filepath=path + ".h5", verbose=1),
        EarlyStopping(monitor='val_loss', patience=args.patience,
                      verbose=0, mode='min')
    ]

    if len(predictions) > 0:
        callbacks.append(PredictionCallback(
            args.modelname, args.trainingname, predictions, args.save_steps))

    model.fit(training_X, training_Y, batch_size=args.batch_size, nb_epoch=args.epochs, validation_data=(testing_X, testing_Y),
              callbacks=callbacks)
