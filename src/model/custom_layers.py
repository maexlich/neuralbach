import numpy as np
from theano import tensor as T
import theano
import keras.backend as K
from keras.engine import Layer


"""Implement lambda layer (Keras) here, to do convolution. No learning"""


def conv1d_md(input, filter, input_shape=(10, 200),
              filter_length=13, filter_flip=True):
    """Convolves 1D vectors (shape=batchsize, vector-length) with a provided convolution mask.

    Args:
        input: Matrix of shape (batchsize, length)
        filter: Matrix of shape (batchsize, filterlength)
        input_shape: shape of input
        filter_length: filterlength
        filter_flip: correlation vs. convolution

    Returns:
        Matrix of shape (batchsize, length-filterlength+1)

    """
    batch_size, input_length = input_shape

    if batch_size is None:
        batch_size = -1

    output_length = input_length - filter_length + 1
    output_shape = (batch_size, output_length)

    filter_flipped = filter[:, ::-1] if filter_flip else filter

    conved = T.zeros(output_shape)  # shape = (batch_size, output_length)

    for num in range(filter_length):
        stack_height = (input_length - num) // filter_length

        # cut input at n*'length' and stack pieces -> performance
        r_input_shape = (batch_size, stack_height, filter_length)
        r_input = input[:, num:stack_height *
                        filter_length + num].reshape(r_input_shape)

        r_conved = T.tensordot(r_input, filter_flipped, (2, 1))
        conved = T.set_subtensor(
            conved[:, num::filter_length], r_conved[:, :, 0])
    return conved


from keras.engine import InputSpec


class TimeDistributed1DConvolution(Layer):
    """1D convolution on every time slice of input sequence.

    Apply a 1D convolution on every time slice of the input sequence and learn the convolution mask.

    This implementation is based on the class :class:`keras.engine.Merge`.

    """
    def __init__(self, flip_filter=False, **kwargs):
        self.flip_filter = flip_filter
        kwargs.update({"trainable": False})
        super(TimeDistributed1DConvolution, self).__init__(**kwargs)

    def get_output_shape_for(self, input_shape):
        #print("Called get output shape for ",input_shape)
        #print("Inputspec", [spec.shape for spec in self.input_spec])
        # Check if we get a list as input, no idea why we have to do this
        # Function somehow called two times once with only one input and once with both
        # if type(input_shape) is list:
        #    input_shape = input_shape[0]

        conv_shape = self.input_spec[1].shape
        input_shape = self.input_spec[0].shape

        return input_shape[:-1] + ((input_shape[-1] - conv_shape[-1] + 1), )

    def build(self, input_shapes):
        print("Called build with input shape", input_shapes)
        self.input_spec = [InputSpec(shape=input_shape)
                           for input_shape in input_shapes]

    def call(self, inputs, mask=None):
        if not isinstance(inputs, list) or len(inputs) <= 1:
            raise Exception('Merge must be called on a list of tensors '
                            '(at least 2). Got: ' + str(inputs))
        print("Called!")
        X, conv_masks = inputs
        input_shape = self.input_spec[0].shape
        conv_shape = self.input_spec[1].shape  # (nb_samples, filter_dim)
        # no batch size specified, therefore the layer will be able
        # to process batches of any size
        # we can go with reshape-based implementation for performance
        input_length = input_shape[1]
        if not input_length:
            input_length = K.shape(X)[1]
        # (nb_samples * timesteps, ...)
        X = K.reshape(X, (-1,) + input_shape[2:])

        # Do same thing with convolution masks
        # (nb_samples, input_length, filter_dim)
        conv_masks = K.repeat(conv_masks, input_length)
        # (nb_samples * input_length, filter_dim)
        conv_masks = K.reshape(conv_masks, (-1, conv_shape[-1]))
        # (nb_samples * timesteps, ...)
        y = self.do_convolution(X, conv_masks, conv_shape[-1])

        # (nb_samples, timesteps, ...)
        output_shape = self.get_output_shape_for(input_shape)
        y = K.reshape(y, (-1, input_length) + output_shape[2:])
        return y

    def __call__(self, inputs, mask=None):
        if not isinstance(inputs, list):
            raise Exception('Merge can only be called on a list of tensors, '
                            'not a single tensor. Received: ' + str(inputs))
        if self.built:
            raise Exception('A Merge layer cannot be used more than once, '
                            'please use ' +
                            'the "merge" function instead: ' +
                            '`merged_tensor = merge([tensor_1, tensor2])`.')

        all_keras_tensors = True
        for x in inputs:
            if not hasattr(x, '_keras_history'):
                all_keras_tensors = False
                break

        if all_keras_tensors:
            layers = []
            node_indices = []
            tensor_indices = []
            for x in inputs:
                layer, node_index, tensor_index = x._keras_history
                layers.append(layer)
                node_indices.append(node_index)
                tensor_indices.append(tensor_index)
            self._arguments_validation(layers, node_indices, tensor_indices)
            self.built = True
            self.add_inbound_node(layers, node_indices, tensor_indices)

            outputs = self.inbound_nodes[-1].output_tensors
            return outputs[0]  # merge only returns a single tensor
        else:
            return self.call(inputs, mask)

    def _arguments_validation(self, layers, node_indices, tensor_indices):
        if type(layers) not in {list, tuple} or len(layers) < 2:
            raise Exception('A Merge should only be applied to a list of '
                            'layers with at least 2 elements. Found: ' + str(layers))

        if tensor_indices is None:
            tensor_indices = [None for _ in range(len(layers))]

        input_shapes = []
        for i, layer in enumerate(layers):
            layer_output_shape = layer.get_output_shape_at(node_indices[i])
            if isinstance(layer_output_shape, list):
                # case: the layer has multiple output tensors
                # and we only need a specific one
                layer_output_shape = layer_output_shape[tensor_indices[i]]
            input_shapes.append(layer_output_shape)
        self.build(input_shapes)

    def do_convolution(self, X, conv_masks, filter_length):
        return conv1d_md(X, conv_masks, K.shape(
            X), filter_length, self.flip_filter)

    def get_config(self):
        config = {
            'flip_filter': self.flip_filter,
            # 'filter_length': self.filter_length
        }
        base_config = super(TimeDistributed1DConvolution, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


if __name__ == '__main__':
    print("Testing if custom theano 1D convolution function conv1d_md returns same as numpy convolve")

    sequence = T.fmatrix('sequence')
    mask = T.fmatrix('mask')

    f = theano.function([sequence, mask], conv1d_md(sequence, mask))

    dummy_sequence = np.tile(np.arange(0, 200), (1280, 1)).astype('float32')
    dummy_mask = np.tile([0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
                          0, 0], (10, 1)).astype('float32')

    output = f(dummy_sequence, dummy_mask)

    # Compare to np implementation
    output_np = np.zeros((dummy_sequence.shape[0], dummy_sequence.shape[
                         1] - dummy_mask.shape[1] + 1))
    i = 0
    for seq, m in zip(dummy_sequence, dummy_mask):
        output_np[i] = np.convolve(seq, m, mode='valid')
        i += 1

    if np.allclose(output, output_np):
        print("Functionality of conv1d_md confirmed!")
    else:
        print("Different results!")
