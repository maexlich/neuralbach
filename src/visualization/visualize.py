import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.colors as mcolors
import numpy as np
import music21


def initialize_pianoroll(length=100):
    """Plots a simple empty pianoroll.

    Args:
        length: Length of the piano roll in quarter notes.

    Returns:
        axes handle
    """
    ax = plt.gca()
    # make horizontal rectangles marking the tones on the keyboard
    white_tones = [60, 62, 64, 65, 67, 69, 71]
    left_bound = -length / 20.0
    right_bound = length
    for tone in white_tones:
        for octave in range(-4, 8):
            lower_bound = tone + octave * 12 - 0.5
            upper_bound = tone + octave * 12 + 0.5
            ax.add_patch(patches.Rectangle(
                (left_bound, lower_bound),  # (x,y)
                right_bound - left_bound,  # width
                upper_bound - lower_bound,  # height
                color='white', alpha=0.25)
            )

    # make vertical lines between all quarter notes
    for i in range(0, length):
        ax.axvline(i, color='white', alpha=0.4)
        ax.axvline(-i, color='white', alpha=0.4)

    # axes labeling
    ax.set_ylabel("Tone Pitch")
    ax.set_xlabel("Timestep in quarter notes")

    ax.set_axis_bgcolor('black')
    return ax


def plot_datamatrix(matrix, note_color=(1, 0, 0),
                    tone_range=(0, 100), ax=None):
    """Plots piano datamatrices onto a pianoroll.

    Args:
        matrix: Nx100 numpy matrix, where N are the timesteps (8 per quarter note), and 100 is the pitch dimension
        note_color: Tuple with RGB color of plotted data
        ax: Axis handle where to plot the data. Can be initialised with :func:`src.visualize.initialize_pianoroll`

    Returns: None

    """
    if ax is None:
        ax = initialize_pianoroll()

    # colors is a list of rgba values with increasing intensity
    colors = [(*note_color, i) for i in np.linspace(0, 1, 3)]
    # interpolate other values and create colormap
    alphamap = mcolors.LinearSegmentedColormap.from_list(
        'mycmap', colors, N=100)

    # plot tones
    ax.imshow(matrix.T, aspect='auto', interpolation='nearest',
              cmap=alphamap, extent=[0, matrix.shape[
                  0] / 8.0, -0.5, matrix.shape[1] - 0.5],
              origin='lower', zorder=10)

    # Calculate extent of tones and limit to tone_range
    lowest_tone = (matrix.mean(0) > 0).argmax()
    highest_tone = matrix.shape[1] - (matrix.mean(0) > 0)[::-1].argmax()
    ax.set_ylim((max(lowest_tone, tone_range[0]), min(
        highest_tone, tone_range[1])))


def join_hold_and_onset(voice):
    return np.maximum(voice[:, :100], voice[:, 100:])


def plot_two_voices(voice1, voice2, color1, color2,
                    tone_range=(0, 100), ax=None):
    if ax is None:
        ax = initialize_pianoroll(voice1.shape[0] // 8)

    plot_datamatrix(voice1, color1, tone_range=tone_range, ax=ax)
    plot_datamatrix(voice2, color2, tone_range=tone_range, ax=ax)
    return ax


def plot_chorals_with_prediction(original, prediction, pred_n=0, offset=26):
    """Creates a plot to visualise predictions of bach chorals, containing predicted and original voices.

    Args:
        original: A matrix with shape (4, sequence_length). Contains 4 voices with integer pitches
        prediction: A vector with shape (sequence_length) Contrains predicted new voice
        pred_n: Integer. Which voice does the prediction correspond to?
    """
    squeezed = np.squeeze(original).copy()
    # shift up because training data was shifted down
    squeezed[squeezed != 0] += offset
    max_t = np.where(original.sum(0) == 0)[0][0]
    squeezed = squeezed[:, 0:max_t]

    colors = plt.cm.rainbow(np.linspace(0, 1, len(squeezed)))

    for i, voice in enumerate(squeezed):
        plt.plot(voice, linewidth=2, label='Voice %s' % i, c=colors[i])

    prediction_sq = np.squeeze(prediction).copy()
    # shift up because training data was shifted down
    prediction_sq[prediction_sq != 0] += offset
    prediction_sq = prediction_sq[0:max_t]

    plt.plot(prediction_sq, linewidth=2, label='Voice %s pred.' %
             pred_n, c=colors[pred_n], linestyle='dotted')
    plt.xlabel('Timeslices in 1/4 of quarter note')
    plt.ylabel('MIDI pitches')
    plt.legend(loc=3)


def chorals_to_music21(chorals, output='test.mid', offset=26):
    """Converts the numpy representation of a choral into a midi file which can be played later.

    Args:
        chorals: numpy array with shape (n_voices, len_sequence), first row will be played by trumpet, others by violin
        output: filename where to save midi
        offset: by which integer to offset the chorals pitches
    """
    chorals = np.squeeze(chorals).copy()
    max_t = np.where((chorals > 0).sum(0) <= 1)[0][0]
    chorals[chorals != 0] = chorals[chorals != 0] + offset
    chorals = chorals[:, 0:max_t]
    all = music21.stream.Stream()
    for choral in chorals:
        s = music21.stream.Part()
        pitch_changes = np.hstack(([0], np.diff(choral)))
        note_starts = np.hstack(([0], np.where(pitch_changes != 0)[0]))
        note_ends = np.hstack((note_starts[1:], [len(choral)]))
        for start, end in zip(note_starts, note_ends):
            pitch = choral[start]
            if pitch != 0:
                n = music21.note.Note(pitch, quarterLength=(end - start) / 4.)
            else:
                n = music21.note.Rest(quarterLength=(end - start) / 4.)
            s.append(n)
        all.append(s)
    for part in all[1:]:
        part.insert(0, music21.instrument.Violin())
    all[0].insert(0, music21.instrument.Trumpet())
    mf = music21.midi.translate.streamToMidiFile(all)
    mf.open(output, 'wb')
    mf.write()
    mf.close()
