import numpy as np


def track2recarray(track):
    """Converts a :class:`mido.MidiTrack` to a numpy recarray with continuous time (1=quarter note)

    Args:
        track (:class:`mido.MidiTrack`): The track that should be converted

    Returns:
        :class:`numpy.recarray`:
        Converted track with columns time (float), note (uint8), velocity (uint8) and type (bool)

        type == False resembles a note_off message while type == True resembles a note_on message
    """
    action_dict = {'note_off': 0, 'note_on': 1}
    messagelist = [m for m in track if hasattr(
        m, 'time') and hasattr(m, 'note')]
    real_time_in_quarter_notes = np.cumsum(
        [m.time for m in messagelist]) / 480.

    return np.rec.array([(t, m.note, m.velocity, action_dict[m.type])
                         for m, t in zip(messagelist, real_time_in_quarter_notes)],
                        dtype=[('time', float), ('note', 'uint8'), ('velocity', 'uint8'), ('type', bool)])


def recarray2datamatrix(
        recarray, resolution_per_quarter_note=8, min_tone=0, max_tone=100):
    """Converts a recarray with midi messages to a binary matrix with vertical time and horizontal notes

    Args:
        recarray (:class:`numpy.recarray`): A rectarray as generated with :method:`track2recarray`
        resolution_per_quarter_note (:obj:`int`, optional, default=8): how many time ticks should be set for one quater note
        min_tone (:obj:`int`, optional, default=0): highest expected tone
        max_tone (:obj:`int`, optional, default=100): lowest expected note

    Returns:
        :obj:2 element tuple containing numpy arrays with dimensions (quantized length of track, __max_tone__-__min_tone__)

        - **onset_matrix** (:class:`numpy.ndarray`): indicates when a note is touched, contains the velocity of touch
        - **hold_matrix** (:class:`numpy.ndarray`): indicates a held tone after touched

    """
    length_in_quarter_notes = np.ceil(recarray.time.max()).astype('int')
    tone_range = max_tone - min_tone

    # fix buggy midi files
    recarray.note[recarray.note > 96] = 96

    # Instanciate matrices
    onset_matrix = np.zeros(
        (length_in_quarter_notes * resolution_per_quarter_note, tone_range), dtype='uint8')
    hold_matrix = np.zeros(
        (length_in_quarter_notes * resolution_per_quarter_note, tone_range), dtype='uint8')

    # Fill onset_matrix
    onset_messages = recarray[recarray.type == True]
    onset_timings_quantised = np.round(onset_messages.time * 8).astype('int')

    onset_matrix[onset_timings_quantised,
                 onset_messages.note] = onset_messages.velocity

    # Fill hold_matrix
    notes_existing = np.unique(recarray.note)
    for note in notes_existing:
        note_messages = recarray[recarray.note == note]
        note_onset_messages = note_messages[note_messages.type == True]
        for i, j in zip(note_messages[::2], note_messages[1::2]):
            starttime = np.round(i.time * 8.0).astype('int')
            stoptime = np.round(j.time * 8.0).astype('int')
            hold_matrix[starttime:stoptime, note] = i.velocity

    return onset_matrix, hold_matrix
