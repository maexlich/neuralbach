# -*- coding: utf-8 -*-
"""Management of training and testing datasets.

By calling this file, the datasets required for training and testing of the models are created.
A individual dataset is stored under the path data/processed/{datasetname}_{track}.npy

Example:
    Execute with correctly setup pythonpath to create datasets::

        $ PYTHONPATH=<path to src> python datasets.py


"""

import numpy as np
from os import path
from glob import glob
import music21
from collections import Counter, OrderedDict


def do_dataset_action(name, action, *positional, **optional):
    """Function that performs an action on one of the datasets.

    Possible actions are:
        * "create" to read the dataset from the original input files and store it in the later used format for training
        * "get_training" returns the training portion of the dataset
        * "get_testing" returns the testing portion of the dataset

    The actions are defined in the dictionary datasets in the following manner:

        datasets = {
            datasetname: {
                create: (createfunction, parameters),
                get_training: (trainingdatafunction, parameters)
            }
        }

    Args:
        name (str): name of dataset
        action (str): action that should be performed on dataset
        *args: additional parameters
        **optional:

    Returns:
        dependent on action, None in case of "create", other functions return tuples containing the data

    """
    options_dict = datasets[name]
    options = options_dict[action]
    function = options[0]
    parameters = options[1]
    output = function(name, *parameters, *positional, **optional)
    print("Executed {} on {} dataset!".format(action, name))
    return output


def get_dataset_names():
    return datasetorder


def create_datasets():
    """Create numpy representations of all datasets in the dictionary datasets, by calling the "create" action.
    """
    for datasetname in datasetorder:
        options_dict = datasets[datasetname]
        options = options_dict["create"]
        function = options[0]
        parameters = options[1]
        function(datasetname, *parameters)
        print("Created {} dataset!".format(datasetname))


def create_dataset_from_midi(
        datasetname, midifile, chunksize, n_subsamples, validationfraction):
    """Create hold matrix for midi file and store in numpy file format

    Args:
        name (str): name to prefix file
        midifile: midifile that should be processed

    """
    from mido import MidiFile
    file = MidiFile(midifile)

    track1 = get_track_as_numpy(file.tracks[1])['hold']
    track2 = get_track_as_numpy(file.tracks[2])['hold']

    rtrack1, rtrack2 = get_random_subsequences(
        track1, track2, chunksize, n_subsamples)

    testmask = generate_random_filter_mask(n_subsamples, validationfraction)

    np.save(datasetpattern.format(name=datasetname,
                                  track="testing-track1"), rtrack1[testmask])
    np.save(datasetpattern.format(name=datasetname,
                                  track="testing-track2"), rtrack2[testmask])
    np.save(datasetpattern.format(name=datasetname,
                                  track="training-track1"), rtrack1[np.logical_not(testmask)])
    np.save(datasetpattern.format(name=datasetname,
                                  track="training-track2"), rtrack2[np.logical_not(testmask)])


def create_from_single_midi(datasetname, midifile):
    from mido import MidiFile
    file = MidiFile(midifile)
    track = get_track_as_numpy(file.tracks[1])['hold']
    track = track[np.newaxis, ...]
    np.save(datasetpattern.format(name=datasetname, track="testing"), track)


def get_track_as_numpy(track):
    """Convert the provided track as a dictionary of numpy arrays containing onset and hold matrix

    Args:
        track (:class:`mido.MidiTrack`): the track that should be stored

    Returns:
        :class:`dict`: of :class:`numpy.rec.recarray` containing onset and hold data of provided :class:`mido.MidiTrack`

    """
    from src.data.midi2matrix import track2recarray, recarray2datamatrix
    recarray = track2recarray(track)
    track_onset, track_hold = recarray2datamatrix(recarray)
    return {'onset': track_onset, 'hold': track_hold}


def get_random_subsequences(track1, track2, chunksize, n_subseq):
    random_subset_integers = np.random.randint(
        0, len(track1) - chunksize, n_subseq)
    track1_random_subseq = np.array(
        [track1[i:i + chunksize] for i in random_subset_integers])
    track2_random_subseq = np.array(
        [track2[i:i + chunksize] for i in random_subset_integers])

    return track1_random_subseq, track2_random_subseq


def create_dataset_from_midi_folder(
        datasetname, folder, chunksize, n_subsamples, validationfraction):
    track1 = []
    track2 = []
    for midifile in glob(path.join(folder, "*.mid")):
        from mido import MidiFile
        file = MidiFile(midifile)
        t1 = get_track_as_numpy(file.tracks[1])
        t2 = get_track_as_numpy(file.tracks[2])

        t1 = t1['hold']
        t2 = t2['hold']

        total_length = max(t1.shape[0], t2.shape[0])

        missing = total_length - t1.shape[0]
        t1 = np.concatenate([t1, np.zeros((missing, 100))], axis=0)
        missing = total_length - t2.shape[0]
        t2 = np.concatenate([t2, np.zeros((missing, 100))], axis=0)

        track1.append(t1)
        track2.append(t2)

    track1 = np.concatenate(track1)
    track2 = np.concatenate(track2)

    rtrack1, rtrack2 = get_random_subsequences(
        track1, track2, chunksize, n_subsamples)

    testmask = generate_random_filter_mask(n_subsamples, validationfraction)

    np.save(datasetpattern.format(name=datasetname,
                                  track="testing-track1"), rtrack1[testmask])
    np.save(datasetpattern.format(name=datasetname,
                                  track="testing-track2"), rtrack2[testmask])
    np.save(datasetpattern.format(name=datasetname,
                                  track="training-track1"), rtrack1[np.logical_not(testmask)])
    np.save(datasetpattern.format(name=datasetname,
                                  track="training-track2"), rtrack2[np.logical_not(testmask)])


def execute_command_on_tuple_structure(func, *data):
    """Recursively apply a command on every element of a tuple structure

    Args:
        func (:class:`function`): function to be applied
        *data (:class:`tuple`): tuple structure

    Returns:
        :class:`tuple`: Tuple structure similar to data with func applied to every element.

    """
    output = tuple()
    for element in data:
        if isinstance(element, tuple):
            output += (execute_command_on_tuple_structure(func, *element),)
        else:
            output += (func(element), )
    return output if len(output) > 1 else output[0]


def load_tracks_from_npy(file):
    """Loads a dataset from a numpy file created using :func:`src.data.midi2track.recarray2datamatrix`

    Args:
        file (str, :class:`file`): File created using numpy save

    Returns:
        :class:`numpy.ndarray`: Array containing onset and holds of tones

    """
    data = np.load(file)
    return data.astype(np.bool)


def read_2track_dataset(name):
    """Extracts the dataset with name from preprocessed files.

    Returns:
        :class:`numpy.ndarray`: tuple of first and second track

    """
    track1path = datasetpattern.format(name=name, track="track1")
    track2path = datasetpattern.format(name=name, track="track2")
    return load_tracks_from_npy(track1path), load_tracks_from_npy(track2path)


def create_choral_dataset(
        name, filepath, quantisation_per_quarter_note, trainvoices, validationfraction):
    all_chorals = []

    for choral_num, choral_path in enumerate(
            glob(path.join(filepath, "*.krn"))):
        choral = music21.converter.parse(choral_path)

        assert len(choral.parts) == 4

        choral_length = int(choral.duration.quarterLength)
        choral_numpy = np.zeros(
            (4, choral_length * quantisation_per_quarter_note), dtype='uint8')

        keys = []
        for voice in choral.parts:
            # Find key for all 4 voices
            try:
                keys.append(voice.flat.getElementsByClass(
                    music21.key.Key)[0].tonic)
            except:
                keys.append(voice.analyze('key').tonic)

        key = Counter(keys).most_common(1)[0][0]

        transpose_interval = music21.interval.Interval(
            key, music21.pitch.Pitch('C'))

        for num, voice in enumerate(choral.parts):
            # Fill numpy array with data
            notes = voice.flat.getElementsByClass('Note')
            notes_transp = notes.transpose(transpose_interval)

            midi_pitches = [
                i.element.pitch.midi for i in notes_transp.offsetMap]
            start_times = [int(i.offset * quantisation_per_quarter_note)
                           for i in notes_transp.offsetMap]
            end_times = [int(np.ceil(i.endTime * quantisation_per_quarter_note))
                         for i in notes_transp.offsetMap]

            for pitch, start, end in zip(midi_pitches, start_times, end_times):
                choral_numpy[num, start:end] = pitch
        all_chorals.append(choral_numpy)
        #print("Analysed Choral %s and appended to numpy array." % (choral_num))

    # Pad shorter chorals
    padding_length = max([i.shape[1] for i in all_chorals])

    all_chorals_np = np.zeros((len(all_chorals), 4, padding_length, 1))
    for i, line in enumerate(all_chorals):
        all_chorals_np[i, :, 0:len(line[0]), 0] = line

    lowest_pitch = all_chorals_np[all_chorals_np != 0].min()
    print("Got lowest pitch of {}".format(lowest_pitch))
    # Lowest pitch in data is 27, so we can reduce all data by 26!
    all_chorals_np[all_chorals_np != 0] -= (lowest_pitch - 1)

    testmask = generate_random_filter_mask(
        all_chorals_np.shape[0], validationfraction)
    trainmask = np.logical_not(testmask)

    Xmask = np.zeros(all_chorals_np.shape[1], dtype=bool)
    Xmask[trainvoices] = True
    ymask = np.logical_not(Xmask)

    np.save(datasetpattern.format(name=name, track='testing-X'),
            all_chorals_np[np.ix_(testmask, Xmask)])
    np.save(datasetpattern.format(name=name, track='testing-y'),
            all_chorals_np[np.ix_(testmask, ymask)])

    np.save(datasetpattern.format(name=name, track='training-X'),
            all_chorals_np[np.ix_(trainmask, Xmask)])
    np.save(datasetpattern.format(name=name, track='training-y'),
            all_chorals_np[np.ix_(trainmask, ymask)])


def convert_chorls_to_onehot(chorals, max_pitch):
    all_chorals_np_onehot = np.zeros(
        chorals.shape[:-1] + (max_pitch + 1,), dtype=bool)
    for i, j, k, l in zip(*np.where(chorals != 0)):
        pitch = int(chorals[i, j, k, l])
        all_chorals_np_onehot[i, j, k, pitch] = 1
    return all_chorals_np_onehot


def create_onehot_from_regression(outputname, inputdataset):
    """Create a clasification dataset based on a regression dataset.

    Convert the integer values of thee regression dataset into a dataset of onehot vectors

    Args:
        outputname (str): dataset name for storage
        inputdataset (str): the regression dataset used as a basis for creation

    """
    data = {
        "training": do_dataset_action(inputdataset, 'get_training', squeeze=False),
        "testing": do_dataset_action(inputdataset, 'get_testing', squeeze=False)
    }

    chorals = [np.concatenate(X + [y], axis=1) for X, y in data.values()]
    all_chorals_np = np.concatenate(chorals)
    max_pitch = all_chorals_np[all_chorals_np != 0].max()
    print("Got Max pitch of {}".format(max_pitch))

    for subset, (X, y) in data.items():
        X = np.concatenate(X, axis=1)
        conv_X = convert_chorls_to_onehot(X, max_pitch)
        conv_y = convert_chorls_to_onehot(y, max_pitch)
        np.save(datasetpattern.format(
            name=outputname, track=subset + "-X"), conv_X)
        np.save(datasetpattern.format(
            name=outputname, track=subset + "-y"), conv_y)


def read_numpyfiles(name, *tracks):
    def loader(track):
        return np.load(datasetpattern.format(name=name, track=track))

    return execute_command_on_tuple_structure(loader, tracks)


def generate_random_filter_mask(length, validationfraction):
    testdata = np.random.randint(0, length, length * validationfraction)
    testmask = np.zeros(length, dtype=bool)
    testmask[testdata] = True
    return testmask


def read_and_split_along_axis(name, *params, squeeze=True):
    axis = params[-1]
    files = params[:-1]
    output = tuple()
    for file in files:
        data = np.load(datasetpattern.format(name=name, track=file))
        splitted = np.split(data, data.shape[axis], axis=axis)
        if squeeze:
            splitted = [np.squeeze(split, axis=axis) for split in splitted]
        output += (splitted, ) if len(splitted) > 1 else (splitted[0], )
    return output


datasetpattern = 'data/processed/{name}_{track}.npy'

datasets = {
    "goldberg": {
        "create": (create_dataset_from_midi, ('data/interim/goldberg.mid', 256, 8000, 0.05)),
        "get_testing": (read_numpyfiles, ("testing-track1", "testing-track2")),
        "get_training": (read_numpyfiles, ("training-track1", "training-track2"))
    },
    "beethoven_sonata": {
        "create": (create_dataset_from_midi_folder, ('data/interim/beethoven_sonata', 256, 8000, 0.05)),
        "get_testing": (read_numpyfiles, ("testing-track1", "testing-track2")),
        "get_training": (read_numpyfiles, ("training-track1", "training-track2"))
    },
    "bach_chorals_regression": {
        "create": (create_choral_dataset, ('data/raw/371chorales', 4, [1, 2, 3], 0.05)),
        "get_training": (read_and_split_along_axis, ('training-X', 'training-y', 1)),
        "get_testing": (read_and_split_along_axis, ('testing-X', 'testing-y', 1))
    },
    "bach_chorals_classification": {
        "create": (create_onehot_from_regression, ("bach_chorals_regression", )),
        "get_training": (read_and_split_along_axis, ('training-X', 'training-y', 1)),
        "get_testing": (read_and_split_along_axis, ('testing-X', 'testing-y', 1))
    },
    "odetojoy": {
        "create": (create_from_single_midi, ('data/interim/ode-to-joy.mid', )),
        "get_training": (lambda a: None, tuple()),
        "get_testing": (read_numpyfiles, ('testing',))
    }
}

datasetorder = ["goldberg", "beethoven_sonata",
                "bach_chorals_regression", "bach_chorals_classification", "odetojoy"]


if __name__ == "__main__":
    # parser.add_argument("--chunk-size", dest='chunk_size', type=int, default=256,
    #                    help='Size of a chunk used for training (default: %(default)s)')
    # parser.add_argument("--validation-split", dest='validation_split', type=float, default=0.05,
    # help='Ratio of validation data (default: %(default)s)')
    create_datasets()
    for dataset in datasets.keys():
        print(dataset)
        do_dataset_action(dataset, "get_training")
        do_dataset_action(dataset, "get_testing")
