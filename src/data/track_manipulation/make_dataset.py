# -*- coding: utf-8 -*-
import logging
import os
from glob import iglob
from os.path import join, exists, basename

from dotenv import find_dotenv, load_dotenv

from .midi_processing import *

defunct_midifiles = ['airgstr4.mid', 'fugue11.mid', 'fugue13.mid',
                     'fugue15.mid', 'fugue19.mid', 'fugue8.mid', 'prelude2.mid']


def copy_files_by_tracknumber(input_path, output_path):
    if not exists(output_path):
        os.mkdir(output_path)
    for file in iglob(join(input_path, "**", "*.mid"), recursive=True):
        # Some files cause problems with mido, thus exclude them
        if basename(file) not in defunct_midifiles:
            try:
                nr_tracks = analyse_tracknr(file)
            except Exception as e:
                defunct_midifiles.append(basename(file))
                continue

            targetdir = join(output_path, str(nr_tracks))
            if not exists(targetdir):
                os.mkdir(targetdir)
            copyfile(file, join(targetdir, basename(file)))
    print(defunct_midifiles)


def create_two_voices(input_path, output_path):
    if not exists(output_path):
        os.mkdir(output_path)
    for nr_tracks in iglob(join(input_path, "*")):
        trackcount = basename(nr_tracks)
        if trackcount in function_mapping:
            for file in iglob(join(nr_tracks, "*.mid")):
                output = join(output_path, basename(file))
                function_mapping[trackcount](file, output)


def main():
    logger = logging.getLogger(__name__)
    inputpath = join("data", "external")
    intermediatepath = join("data", "interim")

    sorted_files = join(intermediatepath, "sorted")
    tranformed_files = join(intermediatepath, "transformed")

    logger.info("Copying files to different directories based on track number")
    copy_files_by_tracknumber(inputpath, sorted_files)
    logger.info('Tranforming midi files to contain two tracks')
    create_two_voices(sorted_files, tranformed_files)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = os.path.join(os.path.dirname(__file__), os.pardir, os.pardir)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
