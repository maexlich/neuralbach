from mido import MidiFile
from mido import MidiTrack
from mido import Message, MetaMessage
from enum import Enum


class ExtractionMethod(Enum):
    tracks = 1
    highestLowest = 2


class AbsoluteTimeTrack(MidiTrack):

    def __init__(self, originalTrack=None):
        super(AbsoluteTimeTrack, self).__init__()
        if originalTrack is not None:
            absoluteTime = 0
            for message in originalTrack:
                absoluteTime += message.time
                message.time = absoluteTime
                super(AbsoluteTimeTrack, self).append(message)
            if self[-1].type == 'note_on':
                self.playing = True
            else:
                self.playing = False
        else:
            self.playing = False

    def shift_times(self, time):
        for instance in self:
            instance.time += time

    def isUncompletedTone(self, message):
        uncompletedTone = False
        for savedMessage in self:
            if isinstance(savedMessage, MetaMessage):
                continue
            if savedMessage.note == message.note:
                if not uncompletedTone and savedMessage.type == 'note_on':
                    uncompletedTone = True
                elif uncompletedTone and savedMessage.type == 'note_off':
                    uncompletedTone = False
                else:
                    print('Found errors in track: Uncompleted Tone: ',
                          uncompletedTone, savedMessage)
        return uncompletedTone

    def append(self, object):
        if object.type == 'note_off':
            if self.isUncompletedTone(object):
                super(AbsoluteTimeTrack, self).append(object)
                self.playing = False
            else:
                print("Tried to append note_off for completed tone, ignoring...")
        elif object.type == 'note_on':
            if not self.playing:
                super(AbsoluteTimeTrack, self).append(object)
                self.playing = True
            else:
                print("Tried to append note_on while already playing tone, ignoring...")
        else:
            print("Something went wrong")

    def get_last_tone(self):
        for item in self[::-1]:
            if not isinstance(item, MetaMessage):
                return item
        # Return dummy message
        return Message(type='note_off')

    def get_first_tone(self):
        for item in self:
            if not isinstance(item, MetaMessage):
                return item
        # Return dummy message
        return Message(type='note_off')

    def extend(self, objects):
        if objects.get_first_tone().type == 'note_off' and self.isUncompletedTone(
                objects.get_first_tone()):
            super(AbsoluteTimeTrack, self).extend(objects)
            self.playing = True if objects.get_last_tone().type == 'note_on' else False
        elif objects.get_first_tone().type == 'note_on':
            super(AbsoluteTimeTrack, self).extend(objects)
            self.playing = True if objects.get_last_tone().type == 'note_on' else False
        else:
            print('Ignoring request to extend')

    def get_total_time(self):
        return self[-1].time

    def isSameNote(self, message):
        return self[-1].note == message.note

    def getRelativeTrack(self):
        lastplayed = 0
        out = MidiTrack()
        for message in sorted(self, key=lambda x: x.time):
            message.time -= lastplayed
            lastplayed += message.time
            out.append(message)
        return out


class VoiceExtractor():

    def __init__(
            self, file, extraction_method=ExtractionMethod.tracks, **kwargs):
        self.file = MidiFile(file, **kwargs)
        self.method = extraction_method
        print("Created VoiceExtractor for file type:", self.file.type)

    def getNrOfVoices(self, max):
        if self.method == ExtractionMethod.tracks:
            return len(self.file.tracks)
        elif self.method == ExtractionMethod.highestLowest:
            return 2
        return 2  # nr of voices detected

    def getTracks(self, filters=[]):
        if self.method is ExtractionMethod.tracks:
            return self.file.tracks if len(
                filters) else self.file.tracks[filters]
        elif self.method is ExtractionMethod.highestLowest:
            return self.__extractHighLowTracks()

    def __getLongestTrack(self):
        longest = MidiTrack()
        for track in self.file.tracks:
            if len(track) > len(longest):
                longest = track

        return longest

    def __extractHighLowTracks(self):
        absTrack = sorted(AbsoluteTimeTrack(self.__getLongestTrack()), key=lambda x: (
            x.time, 0 if x.type == 'note_off' else 1))

        high = AbsoluteTimeTrack()
        high.name = "Right Hand"

        low = AbsoluteTimeTrack()
        low.name = "Left Hand"

        unclear = AbsoluteTimeTrack()

        for message in absTrack:
            # Dont regard MetaMessages
            if not isinstance(message, MetaMessage):
                    # check if a note is switched off
                if message.type == 'note_off':
                    if high.isUncompletedTone(message):
                        high.append(message)
                    elif low.isUncompletedTone(message):
                        low.append(message)
                    elif unclear.isUncompletedTone(message):
                        unclear.append(message)
                    else:
                        print("Unable to classify message:", message)

                # check if we are staring to play a note
                elif message.type == 'note_on':
                    if (not high.playing) and (not low.playing):
                        if not unclear.playing:
                            #print("Appending to unclear track: ", message)
                            unclear.append(message)
                        else:
                            if unclear.get_last_tone().note <= message.note:
                                #print("Moving unclear to low")
                                high.append(message)
                                low.extend(unclear)
                            elif unclear.get_last_tone().note > message.note:
                                #print("Moving unclear to high")
                                low.append(message)
                                high.extend(unclear)
                            unclear = AbsoluteTimeTrack()
                    elif high.playing and (high[-1].note > message.note) and (not low.playing):
                        low.append(message)
                    elif low.playing and (low[-1].note < message.note) and (not high.playing):
                        high.append(message)
                    else:
                        print("Unable to classify message:", message)

        return [high.getRelativeTrack(), low.getRelativeTrack()]
