import mido


def analyse_tracknr(filename):
    thefile = mido.MidiFile(filename)
    # Filter out meta tracks containing meta information
    count = 0
    for track in thefile.tracks:
        notefound = False
        for message in track:
            if not isinstance(message, mido.MetaMessage):
                notefound = True
                continue

        if notefound:
            count += 1

    return count


def split_track(inputfile, outputpath):
    from src.data.track_manipulation.voice_extractor import VoiceExtractor, ExtractionMethod
    extractor = VoiceExtractor(inputfile, ExtractionMethod.highestLowest)

    highvoice, lowvoice = extractor.getTracks()

    with mido.MidiFile(type=1) as mid:
        mid.tracks.append(highvoice)
        mid.tracks.append(lowvoice)
        mid.ticks_per_beat = extractor.file.ticks_per_beat
        mid.save(outputpath)


def rename_tracks(inputfile, outputpath):
    thefile = mido.MidiFile(inputfile)
    trackinfo = dict()
    for no, track in enumerate(thefile.tracks):
        trackinfo[no] = {"sum": 0, "count": 0}
        for message in track:
            if not isinstance(
                    message, mido.MetaMessage) and message.type == 'note_on':
                trackinfo[no]["sum"] += message.note
                trackinfo[no]["count"] += 1

    stats = {}
    for no, track in trackinfo.items():
        if track["count"] > 0:
            stats[no] = track["sum"] / track["count"]

    import operator
    high, highvalue = max(stats.items(), key=operator.itemgetter(1))

    low, lowvalue = min(stats.items(), key=operator.itemgetter(1))
    print("Setting track {} (average note: {}) as Right Hand and track {} (average note: {}) as Left Hand".format(
        high, highvalue, low, lowvalue))
    thefile.tracks[high].name = "Right Hand"
    thefile.tracks[low].name = "Left Hand"
    thefile.save(outputpath)


def extract_2tracks_from_3(inputfile, outputpath):
    thefile = mido.MidiFile(inputfile)
    tracknames = []
    for track in thefile.tracks:
        if not isinstance(track, mido.MetaMessage):
            tracknames.append(track.name)

    if "Right Hand" in tracknames and "Left Hand" in tracknames and "Pedal" in tracknames:
        with mido.MidiFile(type=1) as mid:
            for track in thefile.tracks:
                if not isinstance(
                        track, mido.MetaMessage) and "Pedal" != track.name:
                    mid.tracks.append(track)
            mid.ticks_per_beat = thefile.ticks_per_beat
            mid.save(outputpath)

function_mapping = {
    "1": split_track,
    "2": rename_tracks,
    "3": extract_2tracks_from_3
}
