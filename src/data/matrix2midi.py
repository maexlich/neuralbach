import numpy as np
from mido.midifiles import MidiTrack, Message, MidiFile


def datamatrix2stringtrack(matrix, resolution_per_quarter=2, cutoff=0.35):
    # elementwise maximum
    matrix_downsampled = matrix.reshape(
        8 // resolution_per_quarter, -1, matrix.shape[-1]).mean(0)

    track = MidiTrack()

    # 49=Ensemble Strings 1
    track.append(Message('program_change', program=49))

    # 480 are pulses per quarter note
    delta = int(480 // resolution_per_quarter)

    current_delta = 0
    for timestep, note_vector in enumerate(matrix_downsampled):
        for note in np.where(note_vector > cutoff)[0]:
            track.append(Message('note_on', note=int(note), velocity=int(
                127 * note_vector[note]), time=current_delta))
            current_delta = 0
        current_delta += delta
        for note in np.where(note_vector > cutoff)[0]:
            track.append(Message('note_off', note=int(note), velocity=int(
                127 * note_vector[note]), time=current_delta))
            current_delta = 0
    return track


def make_midi_from_tracks(tracks, outputpath=None):
    with MidiFile(type=1) as mid:
        for track in tracks:
            mid.tracks.append(track)
        mid.ticks_per_beat = 480
        if outputpath:
            mid.save(outputpath)
        else:
            return mid

if __name__ == '__main__':
    from src.data.datasets import get_test_data
    from src.model.generic import load_model
    from src.visualization.visualize import *
    from src.data.midi2matrix import track2recarray, recarray2datamatrix
    import matplotlib.pyplot as plt

    ode = get_test_data('odetojoy').astype(bool)
    model = load_model('convolutional', 'bqtrained')

    ode_second_voice = model.predict(np.array([ode]))[0]

    track1 = MidiFile('data/interim/ode-to-joy.mid').tracks[1]
    track2 = datamatrix2stringtrack(
        ode_second_voice[:, 0:100], ode_second_voice[:, 100:200])

    ax = initialize_pianoroll(len(ode))
    plot_datamatrix(np.maximum(ode_second_voice[:, 0:100], ode_second_voice[
                    :, 100:200]), (1, 0, 0), ax=ax)
    plot_datamatrix(ode[:, 100:200], (0, 1, 0), ax=ax)

    plt.show()

    make_midi_from_tracks([track1, track2], 'odetojoy_.mid')
