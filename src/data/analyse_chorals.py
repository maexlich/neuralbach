import music21
import numpy as np
from glob import glob
import matplotlib.pyplot as plt

tonelengths = []  # initialize with whole
toneheights = []
lowest = 88

for choralpath in glob('data/raw/371chorales/*.krn'):
    choral = music21.converter.parse(choralpath)
    for note in choral.flat.getElementsByClass(music21.note.Note):
        tonelengths.append(note.duration.quarterLength)
        toneheights.append(note.pitch.midi)
    print(choralpath)

tonelengths = np.array(tonelengths)
toneheights = np.array(toneheights)
plt.hist(np.log2(tonelengths), 100)
plt.xlabel("log of tone length in quarter notes")
plt.ylabel("#Occurrences")
plt.savefig("reports/figures/bach_choral_lengths.pdf")
print("Shortest tone: ", np.min(tonelengths),
      ", Longest tone: ", np.max(tonelengths))
print("Lowest tone: ", np.min(toneheights),
      ", Highest tone: ", np.max(toneheights))
