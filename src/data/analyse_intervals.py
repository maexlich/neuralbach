import numpy as np
from collections import Counter
import matplotlib.pyplot as plt


all_chorals_np = np.load('data/processed/bach_chorales.npy')
all_chorals_flat = all_chorals_np[:, :, :, 0]

all_chorals_voices = np.transpose(all_chorals_flat, (1, 0, 2)).reshape((4, -1))

nonzero = all_chorals_voices.min(0) != 0
all_chorals_nonzero = np.transpose(all_chorals_voices[:, nonzero])


def cartesian_distance(x):
    return np.subtract.outer(x, x)[np.tril_indices(x.shape[0], k=-1)]

differences = []

for i in all_chorals_nonzero:
    differences.append(cartesian_distance(i))

differences = np.array(differences).flatten()
differences_pos = np.concatenate((-np.abs(differences), np.abs(differences)))
differences_nooctave = differences % 12

counter = Counter(differences_nooctave)

counts = np.array(list(counter.values()))
loss_function = 1 / (counts)
loss_function = 3 * loss_function / loss_function.max() + 0.5
loss_function[0] = 0

f, (ax1, ax2) = plt.subplots(1, 2)
ax1.hist(differences_nooctave, 40)
ax1.set_title('Histogram of intervals between voices in 371 Bach Chorales')
ax1.set_xlabel('Half tone interval')
ax2.plot(np.arange(0, 12), loss_function, linewidth=3)
ax2.set_title(
    'Heuristic for Musical Loss Function (0.5 + 3*(1/counts)/(1/counts).max()')
ax2.set_xlabel('Half tone interval')
