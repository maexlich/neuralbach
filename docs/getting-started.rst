Getting started
===============

All required files are provided with the repository.
To install all the requirements of the software, call the appropreate command `make requirements`.

To convert the midi files and humdrum files of the repository into the numpy representations used during training
call the command `make data`.

The models can be trained by the `make training` command and prediction can be performed with the `make prediction` command.