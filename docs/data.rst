Import and processing of data
=============================

Usage of and methods in datasets.py:
------------------------------------

.. automodule:: src.data.datasets
    :members:

