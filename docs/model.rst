Model Training and prediction
=============================

Model training
--------------

.. argparse::
   :module: src.model.training
   :func: get_parser
   :prog: scr/model/training.py


Methods in src.model.training
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: src.model.training
    :members:

Prediction
----------

.. argparse::
   :module: src.model.prediction
   :func: get_parser
   :prog: src/model/training.py

Methods in src.model.prediction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: src.model.prediction
    :members:

Usage of models from python and implementation
----------------------------------------------

.. automodule:: src.model.generic
    :members:

.. automodule:: src.model.custom_layers
    :members:

.. automodule:: src.model.models
    :members:
