Commands
========

The Makefile contains the central entry points for common tasks related to this project.

* `make requirements` will install the python packages required for this project to function by using pip
* `make data` will read the input data an convert it into the format later used for training.
* `make training` calls src/model/training.py and passes parameters

    See :ref:`Model training` for more details. To use optional arguments pass -- so they are not confused with those of make:

        $ make training modelname trainingname 8 -- --predictions testprediction

* `make prediction` calls src/model/prediction.py and passes parameters

    See :ref:`Prediction` for more details. To use optional arguments pass -- so they are not confused with those of make:

        $ make prediction modelname trainingname predictions -- --some-optional=parameter

* `make notebook` start jupyter notebook server with correct $PYTHONPATH and working dir